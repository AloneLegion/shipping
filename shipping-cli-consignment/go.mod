module gitlab.com/alonelegion/shipping/shipping-cli-consignment

go 1.14

replace gitlab.com/alonelegion/shipping/shipping-service-consignment => ../shipping-service-consignment

// replace google.golang.org/grpc => google.golang.org/grpc v1.26.0

require (
	github.com/golang/protobuf v1.4.2 // indirect
	gitlab.com/alonelegion/shipping/shipping-service-consignment v0.0.0-00010101000000-000000000000 // indirect
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	golang.org/x/sys v0.0.0-20200724161237-0e2f3a69832c // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/genproto v0.0.0-20200726014623-da3ae01ef02d // indirect
	google.golang.org/grpc v1.30.0 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)